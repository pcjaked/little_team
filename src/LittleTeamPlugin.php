<?php namespace Pcjaked\LittleTeam;

use Pcjaked\LittleTeam\FileManager;
use Pcjaked\LittleTeam\Admin\Admin;
use Pcjaked\LittleTeam\Frontend\Frontend;

/**
 * Class LittleTeamPlugin
 *
 * @package Pcjaked\LittleTeam
 */
class LittleTeamPlugin {

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * LittleTeamPlugin constructor.
	 *
     * @param string $mainFile
	 */
    public function __construct($mainFile) {
        $this->fileManager = new FileManager($mainFile);

        add_action('init', [$this, 'loadTextDomain' ], 0);
        add_action('init', [$this, 'registerNewTypes']);
	}

	/**
	 * Run plugin part
	 */
	public function run() {
		if ( is_admin() ) {
			new Admin( $this->fileManager );
		} else {
			new Frontend( $this->fileManager );
		}
	}

    public function registerNewTypes() {
        register_post_type('little_team', [
                'labels' => [
                    'name'               => 'Little team',
                    'singular_name'      => 'Little team',
                    'menu_name'          => 'Little team',
                    'all_items'          => 'Little team',
                    'view_item'          => 'View',
                    'add_new_item'       => 'Add new team',
                    'add_new'            => 'New team',
                    'edit_item'          => 'Edit team',
                    'update_item'        => 'Update team',
                    'search_items'       => 'Find team',
                    'not_found'          => 'Not found team',
                    'not_found_in_trash' => 'Not found in trash',
                ],
                'supports'      => ['title', 'editor', 'author', 'thumbnail'],
                'taxonomies'    => ['little_team_position', 'little_team_organization'],
                'public'        => true,
                'menu_position' => 5,
                'menu_icon'     => 'dashicons-book',
                'hierarchical'  => false,
            ]
        );

        register_taxonomy('little_team_position', ['little_team'], [
            'labels'       => [
                'name'                  => 'Positions',
                'singular_name'         => 'Positions',
                'menu_name'             => 'Positions',
                'all_items'             => 'Positions',
                'new_item_name'         => 'New position',
                'add_new_item'          => 'Add new position',
                'edit_item'             => 'Edit position',
                'update_item'           => 'Update position',
                'search_items'          => 'Find',
                'add_or_remove_items'   => 'Add or delete position',
                'choose_from_most_used' => 'Choose from the most used tags',
                'not_found'             => 'Not found position',
            ],
            'public'       => true,
            'hierarchical' => false,
        ]);

        register_taxonomy('little_team_organization', ['little_team'], [
            'labels'       => [
                'name'                  => 'Organization',
                'singular_name'         => 'Organization',
                'menu_name'             => 'Organization',
                'all_items'             => 'Organization',
                'new_item_name'         => 'New organization',
                'add_new_item'          => 'Add new organization',
                'edit_item'             => 'Edit organization',
                'update_item'           => 'Update organization',
                'search_items'          => 'Find',
                'add_or_remove_items'   => 'Add or delete organization',
                'choose_from_most_used' => 'Choose from the most used tags',
                'not_found'             => 'Not found position',
            ],
            'public'       => true,
            'hierarchical' => false,
        ]);

    }

    /**
     * Load plugin translations
     */
    public function loadTextDomain()
    {
        $name = $this->fileManager->getPluginName();
        load_plugin_textdomain('little-team', false, $name . '/languages/');
    }
}