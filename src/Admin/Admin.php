<?php namespace Pcjaked\LittleTeam\Admin;

use Pcjaked\LittleTeam\FileManager;

/**
 * Class Admin
 *
 * @package Pcjaked\LittleTeam\Admin
 */
class Admin {

	/**
	 * @var FileManager
	 */
	private $fileManager;

	private $metaBoxFields = ['sallary', 'birth_date'];

	/**
	 * Admin constructor.
	 *
	 * Register menu items and handlers
	 *
	 * @param FileManager $fileManager
	 */
	public function __construct( FileManager $fileManager ) {
		$this->fileManager = $fileManager;

		add_action('add_meta_boxes', [$this, 'registerMetaBox']);
		add_action('save_post', [$this, 'saveData']);
	}

    public function registerMetaBox() {
        add_meta_box(
            'little_team_meta_box',
            'Team data',
            [$this, 'metaBox'],
            'little_team',
            'normal',
            'high'
        );
    }

    public function metaBox($post) {
        $data = $this->prepareData();

        foreach ($this->metaBoxFields as $field) {
            $data[$field] = get_post_meta($post->ID, "_little_team_{$field}", true);
        }

        $this->fileManager->includeTemplate('admin/meta-box.php', $data);
    }

    public function saveData($postId) {
        $data = $this->prepareData($_POST);

        foreach ($data as $field => $value) {
            update_post_meta($postId, "_little_team_{$field}", $value);
        }
    }

    private function prepareData($postData = []) {
	    $data = [];

        foreach ($this->metaBoxFields as $field) {
            $data[$field] = isset($postData[$field]) ? $postData[$field] : '';
        }

        return $data;
    }

}