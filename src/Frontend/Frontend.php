<?php namespace Pcjaked\LittleTeam\Frontend;

use Pcjaked\LittleTeam\FileManager;

/**
 * Class Frontend
 *
 * @package Pcjaked\LittleTeam\Frontend
 */
class Frontend {

	/**
	 * @var FileManager
	 */
	private $fileManager;

	public function __construct( FileManager $fileManager ) {
		$this->fileManager = $fileManager;

        add_shortcode('little_team', [$this, 'littleTeamShortCode']);
	}

    public function littleTeamShortCode($atts) {
        $data = shortcode_atts([
            'total' => wp_count_posts('little_team')->publish,
            'view_style' => 'default'
        ], $atts);

        $posts = get_posts(['post_type' => 'little_team']);

        $postsData = [];
        foreach ($posts as $post) {
            $position = array_map(function ($term) {
                return $term->name;
            }, get_the_terms($post->ID, "little_team_position"));

            $organization = array_map(function ($term) {
                return $term->name;
            }, get_the_terms($post->ID, "little_team_organization"));

            $imageUrls = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');

            $postsData[$post->ID] = [
                'post' => $post,
                'sallary' => get_post_meta($post->ID, "_little_team_sallary", true),
                'birth_date' => get_post_meta($post->ID, "_little_team_birth_date", true),
                'position' => implode(', ', $position),
                'organization' => implode(', ', $organization),
                'img_url' => isset($imageUrls[0]) ? $imageUrls[0] : '',
            ];
        }

        $file = $this->fileManager->locateTemplate("frontend/{$data['view_style']}.php");
        $file = file_exists($file) ? $data['view_style'] : 'default';

        $this->fileManager->includeTemplate("frontend/{$file}.php", [
            'total' => $data['total'],
            'posts' => $postsData,
        ]);
    }
}