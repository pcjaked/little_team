<?php

use Pcjaked\LittleTeam\LittleTeamPlugin;

/**
 *
 * Plugin Name:       Little Team
 * Author:            pcjaked
 * Text Domain:       little-team
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

call_user_func( function () {

	require_once plugin_dir_path( __FILE__ ) . 'autoload.php';

	$main = new LittleTeamPlugin( __FILE__ );

	$main->run();
} );