<?php foreach ($posts as $id => $post): ?>
    <h2><?= $post['post']->post_title ?></h2>
    <div>
        Description:  <?= $post['post']->post_content ?><br/>
        Position:     <?= $post['position']; ?><br/>
        Organization: <?= $post['organization']; ?><br/>
        Sallary:      <?= $post['sallary']; ?><br/>
        Birth Date:   <?= $post['birth_date']; ?><br/>

        <?php if ($post['img_url']): ?>
            <img src="<?= $post['img_url'] ?>">
        <?php endif; ?>

        <br/>
    </div>

    <hr>
<?php endforeach; ?>
