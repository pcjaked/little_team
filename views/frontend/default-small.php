<table>
    <?php foreach ($posts as $id => $post): ?>
    <tr>
        <td>
            <h2><?= $post['post']->post_title ?></h2>
            Description:  <?= $post['post']->post_content ?><br/>
            Position:     <?= $post['position']; ?><br/>
            Organization: <?= $post['organization']; ?><br/>
            Sallary:      <?= $post['sallary']; ?><br/>
            Birth Date:   <?= $post['birth_date']; ?><br/>
        </td>
        <td>
            <?php if ($post['img_url']): ?>
                <img src="<?= $post['img_url']; ?>" height="100" width="200">
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>


