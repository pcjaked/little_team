<table class="form-table">
    <tbody>
        <tr>
            <th>
                <label for="sallary">Sallary</label>
            </th>
            <td>
                <input type="number" id="sallary" name="sallary" value="<?= $sallary ?>">
                <p class="description">Team sallary</p>
            </td>
        </tr>

        <tr>
            <th>
                <label for="birth_date">Birth date</label>
            </th>
            <td>
                <input type="date" id="birth_date" name="birth_date" value="<?= $birth_date ?>">
                <p class="description">Team birth date</p>
            </td>
        </tr>
    </tbody>
</table>